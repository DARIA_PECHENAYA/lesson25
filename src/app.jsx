import ReactDOM from 'react-dom';
// import React, { useState } from 'react';
// import ItemsList from './components/ItemsList/ItemsList';
// import { Info } from './components/Info/Info';
// import { CreateUser } from './components/Form/CreateUser';
import React from 'react';
import Columns from './components/Columns/Columns';
import { themes, ThemeContext } from './components/configs/theme';
import { useState } from 'react';
import Form from './components/box/input/Form';



// import {useState} from 'react';

// function App () {
//   const [age, setAge] = useState(unitialState:18);

//   return (
//       <div>
// )
// // }

// const propsValues = {
//   title: "Dasha <3",
//   items: [{
//     title: "test", 
//     enabled: true
//   }, {
//     title: "testtest", 
//     enabled: true
//   },{
//     title: "ttt", 
//     enabled: true
//   },{
//     title: "te", 
//     enabled: true
//   },{
//     title: "st", 
//     enabled: true
//   },{
//     title: "1111", 
//     enabled: true
//   },{
//     title: "test111", 
//     enabled: true
//   }],
//   counter: 0,
// };

ReactDOM.render(
  <div>
    {/* <Info data={propsValues} /> */}
    {/* <ItemsList data={propsValues} /> */}
    {/* <CreateUser/> */}
    <Columns />
  </div>,
  document.getElementById('app')
);



function App() {
  const [theme, setTheme] = useState(themes.light)

  return (
    <ThemeContext.Provider value={themes.light}>
      <div className="App">
          <Form/>
          <button onClick={() => { setTheme() }} type="button">
            Change Theme
          </button>
      </div>

    </ThemeContext.Provider>
      );
}
