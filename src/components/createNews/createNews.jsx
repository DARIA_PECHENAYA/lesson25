import React from 'react';
class CreateNews extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            newsText: ''
        }
    }
    setNewsText(updateValue) {
        this.setState(state: {
            newsText: updateValue
        });
    }

    createNew() {
        if (this.props.addNew) {
            this.props.addNew(this.state.newsText);
        }
    }

        render() {
            return (
                <form>
                    <input value={this / this.state.newsText} onChange={this.setNewsText} />
                    <button type='button' onClick={() => {
                        this.createNew()
                    }}> Add new post 
                    </button>
                </form >
            )
        }
}