import React from 'react';

export class Item extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            title: this.props.data.title,
            enabled: this.props.data.enabled,
            index: this.props.data.index
        };
    }

    render() {
        let className = '';
        if (!this.state.enabled) {
          className += 'disabled';
        }
        return(<li key={this.state.index} className={className}>{this.state.title}</li>);
    }
}