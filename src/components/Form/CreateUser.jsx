import React from 'react';
import styles from './CreateUser.scss';

export class CreateUser extends React.Component {
    constructor(props) {
        super(props);
        this.createUser = this.createUser.bind(this);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeLastName = this.onChangeLastName.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.state = {
            name: '',
            lastName: '',
            email: ''
        }
    }

    createUser() {
        console.log(this.state)
    }

    onChangeName(e) {
        console.log(e.target.value);
        this.setState({
            name: e.target.value
        });
    }

    onChangeLastName(e) {
        console.log(e.target.value);
        this.setState({
            lastName: e.target.value
        });
    }

    onChangeEmail(e) {
        console.log(e.target.value);
        this.setState({
            email: e.target.value
        });
    }

    render() {
        return (
            <form className={styles.red}>
                <input className={styles.text} type='text' required placeholder='name' value={this.state.name} 
                onChange={this.onChangeName} />
                <input className={styles.text} type='text' required placeholder='lastName' value={this.state.lastName}
                onChange={this.onChangeLastName} />
                <input className={styles.text} type='text' required placeholder='email' value={this.state.email} 
                onChange={this.onChangeEmail}/>

                <button className={styles.text} type='button' onClick={this.createUser}>Submit</button>
            </form>
        )
    }
}