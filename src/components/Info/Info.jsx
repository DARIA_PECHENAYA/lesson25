import React from 'react';

export class Info extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            counter: this.props.data.counter,
            text: this.props.data.title
         };
    }
    componentDidMount() {
        setInterval(() => {
            this.setState({
                counter: this.state.counter + 1
            });
        }, 1000);
        setTimeout(() => {
            this.setState({
                text: `${this.state.counter} new Text`
            });
        }, 3000);
    }
    render() {
        return (
            <div>
                <h2>{this.props.data.title}</h2>
                <br/>
                <span>{this.state.counter}</span>
                <br/>
                <span>{this.state.text}</span>
            </div>
        );
    }
}