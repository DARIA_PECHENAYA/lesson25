import React, { useEffect, useState } from 'react';
import styles from './Columns.scss';

export default function Columns() {
    const [count1, setCount1] = useState(0);
    // const [count2, setCount2] = useState(1);
    // const [count3, setCount3] = useState(1);

    // const elementCount3 = useRef(initialValue: null);


    useEffect ( () => {
        console.log (`${count1}`);
    }, [count1]);

    // useEffect ( a: () => {
    //     alert ('alert');
    // }, b: [count2]);

    // useEffect ( a: () => {

    // }, b: [count3]);
    

    return (
        <div className={styles.box}>
            <h1>{count1}</h1>
            <button onClick={ (e) => {
                setCount1 (count1 + 1);
            } }> name </button>
        </div>
    )

}


// export class Columns extends React.Component {
//     constructor(props) {
//         super(props);
//         this.colomns = this.columns.bind(this);
//         this.onChangeName = this.onChangeName.bind(this);
//         this.onChangeLastName = this.onChangeLastName.bind(this);
//         this.onChangeEmail = this.onChangeEmail.bind(this);
//         this.state = {
//             name: '',
//             lastName: '',
//             email: ''
//         }
//     }

//     columns() {
//         console.log(this.state)
//     }

//     onChangeName(e) {
//         console.log(e.target.value);
//         this.setState({
//             name: e.target.value
//         });
//     }

//     onChangeLastName(e) {
//         console.log(e.target.value);
//         this.setState({
//             lastName: e.target.value
//         });
//     }

//     onChangeEmail(e) {
//         console.log(e.target.value);
//         this.setState({
//             email: e.target.value
//         });
//     }

//     render() {
//         return (
//             <div className={styles.box}>
//                 <button required placeholder='1' value={this.state.name} 
//                 onChange={this.onChangeName} />
//                 <button required placeholder='2' value={this.state.lastName}
//                 onChange={this.onChangeLastName} />
//                 <button required placeholder='3' value={this.state.email} 
//                 onChange={this.onChangeEmail}/>
//             </div>
//         )
//     }
// }