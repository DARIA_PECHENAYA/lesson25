import React from 'react';
import SearchPlugin from '../SearchPlugin/SearchPlugin';
import styles from './ItemsList.scss';
import { Item } from '../Item/Item';


class ItemsList extends React.Component {
  constructor(props){
    super(props);
    this.state = { items: this.props.data.items};

    this.filterList = this.filterList.bind(this);
  }

  filterList(text){
    const filteredList = this.props.data.items.map((item) => {
      return {
        title: item.title,
        enabled: item.title.toLowerCase().search(text.toLowerCase()) !== -1
      }
    });
    this.setState({items: filteredList});
  }

  render() {
    return(
      <div className={styles.itemList}>
        <h2>{this.props.data.title}</h2>
        <SearchPlugin filter={this.filterList} />
        <ul>
          {
            this.state.items.map((item, index)=>{
              return (<Item data={item} index={index}/>);
            })
          }
        </ul>
      </div>
      );
  }
}

export default ItemsList;
