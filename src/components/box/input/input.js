import { ThemeContext } from "../configs/theme";
import { useContext } from "react";


export default function Input(props) {
    const theme = useContext(ThemeContext);

    return (
        <ThemeContext.Consumer>
            {theme => (
                <input placeholder={ props.text || 'default'}
                    style={{ color: theme.inputColor, background: theme.background }} />
            )}
        </ThemeContext.Consumer>
    )
}