import React from 'react';

export const themes = {
    light: {
        buttonColor: 'yellow',
        background: 'gray',
        inputColor:'blue'
    },
    dark: {
        buttonColor: 'blue',
        background: 'green',
        inputColor:'white'
    }
};

export const ThemeContext = React.createContext(themes);